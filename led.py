import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)

GPIO.setup(7, GPIO.OUT)

def io(val):
  GPIO.output(7, val)

def itime(s):
  io(True)
  time.sleep(s)
  io(False)
