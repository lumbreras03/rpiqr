import numpy as np
import cv2
import zbar
import cv2.cv as cv
import led

cap = cv2.VideoCapture(0)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    img = cv2.cv.fromarray(gray)

    # Get data for scanner
    width = img.width
    height = img.height
    raw = img.tostring()

    # create a reader
    scanner = zbar.ImageScanner()

    # configure the reader
    scanner.parse_config('enable')

    # Transform the image
    image = zbar.Image(width, height, 'Y800', raw)
    scanner.scan(image)

    # extract results
    for symbol in image:
        # do something useful with results
        print 'decoded', symbol.type, 'symbol', '"%s"' % symbol.data
        if symbol.data == 'nac13k@gmail.com':
          led.itime(5)


    # Display the resulting frame
    cv2.imshow('frame',gray)
    # Press ESC key to exit
    if cv.WaitKey(10) == 27:
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
